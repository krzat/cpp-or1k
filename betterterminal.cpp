#include "betterterminal.h"

sf::Texture sheet;

/// Returns unicode character from keyboard code.
char getUnicode(sf::Keyboard::Key code) {
    switch(code) {
        case sf::Keyboard::Key::Comma: return ',';
        case sf::Keyboard::Key::SemiColon: return ';';
        case sf::Keyboard::Key::Period: return '.';
        default: {
            if(code >= sf::Keyboard::Key::A && code <= sf::Keyboard::Key::Z)
                return (code + 'A');

            if(code >= sf::Keyboard::Key::Num0 && code <= sf::Keyboard::Key::Num9)
                return (code + '0');

            return '\0';
        }
    }
}

sf::Color makeColor(int value) {
    return sf::Color(
                (value >> 16) & 0xFF,
                (value >> 8) & 0xFF,
                (value >> 0) & 0xFF,
                255);
}

std::vector<sf::Color> Colors = {
    makeColor(0x000000),makeColor(0xBB0000),makeColor(0x00BB00),makeColor(0xBBBB00),
    makeColor(0x0000BB),makeColor(0xBB00BB),makeColor(0x00BBBB),makeColor(0xBBBBBB),
    makeColor(0x555555),makeColor(0xFF5555),makeColor(0x55FF55),makeColor(0xFFFF55),
    makeColor(0x5555FF),makeColor(0xFF55FF),makeColor(0x55FFFF),makeColor(0x55FFFF),
};

const int nrows = TerminalRows;
const int ncolumns = TerminalColumns;

// constructor
BetterTerminal::BetterTerminal(UARTDevice& uart){
    this->uartdev = &uart;
    uart.Output = this;

    sheet.loadFromFile("8x16.png");

    ////this->context.textBaseline = top;
    this->cursorvisible = false;
    this->escapetype = 0;
    this->escapestring = "";
    this->cursorx = 0;
    this->cursory = 0;
    this->scrolltop = 0;
    this->scrollbottom = nrows-1;
    this->currentcolor = 0x7;

    std::vector<int> row;
    row.resize(ncolumns);

    screen.resize(nrows,row);

    row.clear();
    row.resize(ncolumns,currentcolor);
    color.resize(nrows,row);

    this->ScreenUpdate();
    this->Blink();
}

void BetterTerminal::Blink() {
    this->cursorvisible = !this->cursorvisible;
    PlotRow(cursory);
    blinking.restart();
};

void BetterTerminal::DeleteRow(int row) {
    for (auto j = 0; j < ncolumns; j++) {
        this->screen[row][j] = 0x0;
        this->color[row][j] = 0x7;
    }
};

void BetterTerminal::DeleteArea(int row, int column, int row2, int column2) {
    for (auto i = row; i <= row2; i++) {
        for (auto j = column; j <= column2; j++) {
            this->screen[i][j] = 0x0;
            this->color[i][j] = 0x7;
        }
    }
};

void BetterTerminal::PlotRow(int row) {
    UNUSED(row);
};

void BetterTerminal::ScreenUpdate() {
    for (auto i = 0; i < nrows; i++) {
        this->PlotRow(i);
    }
};

void BetterTerminal::ScrollDown() {
    if (this->cursory != this->scrolltop) {
        this->cursory--;
        if (this->cursorvisible) {
            this->PlotRow(this->cursory+1); // delete old cursor position
            this->PlotRow(this->cursory); // show new cursor position
        }
        return;
    }
    this->ScrollDown2();
}

void BetterTerminal::ScrollDown2(bool draw) {
    for (auto i = this->scrollbottom-1; i >= this->scrolltop; i--) {
        if (i == nrows-1) continue;
        for (auto j = 0; j < ncolumns; j++) {
            this->screen[i + 1][j] = this->screen[i][j];
            this->color[i + 1][j] = this->color[i][j];
        }
        if (draw) this->PlotRow(i+1);
    }
    this->DeleteRow(this->scrolltop);
    if (draw) this->PlotRow(this->scrolltop);
}

void BetterTerminal::ScrollUp(bool draw) {
    for (auto i = this->scrolltop+1; i <= this->scrollbottom; i++) {
        if (i == 0) continue;
        for (auto j = 0; j < ncolumns; j++) {
            this->screen[i - 1][j] = this->screen[i][j];
            this->color[i - 1][j] = this->color[i][j];
        }
        if (draw) this->PlotRow(i-1);
    }
    this->DeleteRow(this->scrollbottom);
    if (draw) this->PlotRow(this->scrollbottom);
};



void BetterTerminal::LineFeed() {
    if (this->cursory != this->scrollbottom) {
        this->cursory++;
        if (this->cursorvisible) {
            this->PlotRow(this->cursory-1); // delete old cursor position
            this->PlotRow(this->cursory); // show new cursor position
        }
        return;
    }
    this->ScrollUp(true);
};


void BetterTerminal::Scroll(int count) {
    if (count < 0) {
        count = -count;
        for(auto i=0; i<nrows; i++) {
            DeleteRow(i);
        }
    }
    this->ScreenUpdate();
}


void BetterTerminal::ChangeCursor(std::vector<int> &Numbers) {
    switch (Numbers.size()) {
    case 0:
        this->cursorx = 0;
        this->cursory = 0;
        break;
    case 1:
        this->cursory = Numbers[0];
        if (this->cursory) this->cursory--;
        break;
    case 2:
    default:
        // TODO check for boundaries
        this->cursory = Numbers[0];
        this->cursorx = Numbers[1];
        if (this->cursorx) this->cursorx--;
        if (this->cursory) this->cursory--;
        break;
    }
    if (this->cursorx >= ncolumns) this->cursorx = ncolumns - 1;
    if (this->cursory >= nrows) this->cursory = nrows - 1;
};

void BetterTerminal::ChangeColor(std::vector<int> &Numbers) {
    for (auto i = 0; i < (int)Numbers.size(); i++) {
        switch (Numbers[i]) {
        case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37:
            this->currentcolor = (this->currentcolor & (~0x7)) | ((Numbers[i] - 30) & 0x7);
            break;
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47:
            this->currentcolor = (this->currentcolor & (0xFF)) | (((Numbers[i] - 40) & 0x7) << 8);
            break;
        case 0:
            this->currentcolor = 0x7; // reset
            break;
        case 1:
            this->currentcolor |= 10; // brighter foreground colors
            break;
        case 7:
            this->currentcolor = ((this->currentcolor & 0xF) << 8) | (((this->currentcolor >> 8)) & 0xF); // change foreground and background, no brighter colors
            break;
        case 39:
            this->currentcolor = (this->currentcolor & (~0x7)) | 0x7; // set standard foreground color
            break;
        case 49:
            this->currentcolor = this->currentcolor & 0xFF; // set standard background color
            break;
        case 10:
            // reset mapping ?
            break;
        default:
            LOG("Color " << Numbers[i] << " not found");
            break;
        }
    }
};

void BetterTerminal::HandleEscapeSequence() {
    LOG("Escape sequence: " <<this->escapestring);
    if (this->escapestring == "[J") {
        this->DeleteArea(this->cursory, this->cursorx, this->cursory, ncolumns - 1);
        this->DeleteArea(this->cursory + 1, 0., nrows - 1, ncolumns - 1);
        return;
    } else
    if (this->escapestring == "M") {
        this->ScrollDown2(true);
        return;
    }
    // Testing for [x;y;z
    std::string s = this->escapestring;

    if (s[0] != '[') {
        LOG("Escape sequence unknown: " << this->escapestring);
        return; // the short escape sequences must be handled earlier
    }


    if(s.size() > 0)
        s.erase(0,1); //s = s.substr(1); // delete first sign

    auto lastsign = s.back(); //auto lastsign = s.substr(s.length - 1); // extract command

    if(s.size() > 0)
        s.erase(s.size()-1,1); //s = s.substr(0, s.length - 1); // remove command


//    auto numbers = s.split(";"); // if there are multiple numbers, split them
//    if (numbers[0].length == 0) {
//        numbers = [];
//    }
//    // the array must contain of numbers and not strings. Make this sure
//    for (i=0; i<numbers.length; i++) {
//        numbers[i] = Number(numbers[i]);
//    }

    if(lastsign == 'm') {
        LOG(s);
    }

    std::vector<int> numbers;
    std::string buffer;
    for(char code : s) {
        if(code == ';') {
            if(buffer.size() >0)
                numbers.push_back(std::stoi(buffer));
            buffer.clear();
        } else if( code >= '0' & code <= '9')
            buffer += code;
    }

    if(buffer.size() >0)
        numbers.push_back(std::stoi(buffer));

    auto oldcursory = this->cursory; // save current cursor position
    auto count = 0;
    switch(lastsign) {

        case 'l':
            for(auto i=0; i< (int)numbers.size(); i++) {
                switch(numbers[i]) {
                    /*case 4:
                    break;*/
                    default:
                        LOG("Term Parameter unknown " << numbers[i]);
                    break;
                }
            }
            break;

        case 'm': // colors
            this->ChangeColor(numbers);
            return;

        case 'A': // move cursor up
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            this->cursory -= count;
            break;

        case 'B': // move cursor down
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            this->cursory += count;
            break;

        case 'C': // move cursor right
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            this->cursorx += count;
            break;

        case 'D': // move cursor left
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            this->cursorx -= count;
            if (this->cursorx < 0) this->cursorx = 0;
            break;

        case 'E': // move cursor down
            count = numbers.size() ? numbers[0] : 1;
            this->cursory += count;
            this->cursorx = 0;
            break;

        case 'F': // move cursor up
            count = numbers.size() ? numbers[0] : 1;
            this->cursory -= count;
            if (this->cursory < 0) this->cursory = 0;
            this->cursorx = 0;
            break;

        case 'G': // change cursor column
            count = numbers.size() ? numbers[0] : 1;
            this->cursorx = count;
            if (this->cursorx) this->cursorx--;
            break;

        case 'H': // cursor position
        case 'd':
        case 'f':
            this->ChangeCursor(numbers);
            break;

        case 'K': // erase
            count = numbers.size() ? numbers[0] : 1;
            if (!numbers.size()) {
                this->DeleteArea(this->cursory, this->cursorx, this->cursory, ncolumns - 1);
            } else
            if (numbers[0] == 1) {
                this->DeleteArea(this->cursory, 0., this->cursory, this->cursorx);
            } else
            if (numbers[0] == 2) {
                this->DeleteRow(this->cursory);
            }
            break;

        case 'L': { // scroll down
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            auto top = this->scrolltop;
            this->scrolltop = this->cursory;
            if (count == 1) {
                this->ScrollDown2(true);
            } else {
                for (auto j = 0; j < count-1; j++) {
                    this->ScrollDown2(false);
                }
                this->ScrollDown2(true);
            }
            this->scrolltop = top;  }
            break;

        case 'M': {// scroll up
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            auto top = this->scrolltop;
            this->scrolltop = this->cursory;
            if (count == 1) {
                this->ScrollUp(true);
            } else {
                for (auto j = 0; j < count-1; j++) {
                    this->ScrollUp(false);
                }
                this->ScrollUp(true);
            }
            this->scrolltop = top; }
            break;

        case 'P': { /* shift left from cursor and fill with zero */
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            auto n = 0;
            for (auto j = this->cursorx+count; j < ncolumns; j++) {
                this->screen[this->cursory][this->cursorx+n] = this->screen[this->cursory][j];
                this->color[this->cursory][this->cursorx+n] = this->color[this->cursory][j];
                n++;
            }
            this->DeleteArea(this->cursory, ncolumns-count, this->cursory, ncolumns-1);
            this->PlotRow(this->cursory); }
            break;

        case 'r': { // set scrolling region
            if (numbers.size() == 0) {
                this->scrolltop = 0;
                this->scrollbottom = nrows-1;
            } else {
                this->scrolltop = numbers[0];
                this->scrollbottom = numbers[1];
                if (this->scrolltop) this->scrolltop--;
                if (this->scrollbottom) this->scrollbottom--;
            }  }
            return;

        case 'X': { // erase only number of characters in current line
            count = numbers.size() ? numbers[0] : 1;
            if (count == 0) count = 1;
            for (auto j = 0; j < count; j++) {
                this->screen[this->cursory][this->cursorx+j] = 0x0;
            } }
            break;

        default:  {
            LOG("Escape sequence unknown:'" << this->escapestring << "'"); }
        break;
    }

     if (this->cursorvisible) {
        this->PlotRow(this->cursory);
        if (this->cursory != oldcursory) {
            this->PlotRow(oldcursory);
        }
     }
}

void BetterTerminal::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    UNUSED(states);

    target.draw(canvas);
};

void BetterTerminal::ProcessChar(char c) {
    //DebugMessage("Char:" + c + " " +  String.fromCharCode(c));
    // escape sequence (CS)
    if (this->escapetype == 2) {
        this->escapestring += c;
        if ((c >= 64) && (c <= 126)) {
            this->HandleEscapeSequence();
            this->escapetype = 0;
        }
        return;
    }

    // escape sequence
    if ((this->escapetype == 0) && (c == 0x1B)) {
        this->escapetype = 1;
        this->escapestring = "";
        return;
    }

    // starting escape sequence
    if (this->escapetype == 1) {
        this->escapestring += c;
        // Control Sequence Introducer ([)
        if (c == 0x5B) {
            this->escapetype = 2;
            return;
        }
        this->HandleEscapeSequence();
        this->escapetype = 0;
        return;
    }
    switch (c) {
    case 0xA:
        // line feed
        this->LineFeed();
        //DebugMessage("LineFeed");
        return;
    case 0xD:
        // carriage return
        this->cursorx = 0;
        //DebugMessage("Carriage Return");
        return;
    case 0x7:
        // beep
        return;
    case 0x8:
        // back space
        this->cursorx--;
        if (this->cursorx < 0) {
            this->cursorx = 0;
        }
        this->PlotRow(this->cursory);
        //DebugMessage("backspace");
        return;
    case 0x9: {
        // horizontal tab
        //DebugMessage("tab");
        auto spaces = 8 - (this->cursorx&7);
        do
        {
            if (this->cursorx >= ncolumns) {
                this->PlotRow(this->cursory);
                this->LineFeed();
                this->cursorx = 0;
            }
            this->screen[this->cursory][this->cursorx] = 32;
            this->color[this->cursory][this->cursorx] = this->currentcolor;
            this->cursorx++;
        } while(spaces--);
        this->PlotRow(this->cursory); }
        return;

    case 0x00:   case 0x01:  case 0x02:  case 0x03:
    case 0x04:  case 0x05:  case 0x06:  case 0x0B:
    case 0x0C:  case 0x0E:  case 0x0F:
    case 0x10:  case 0x11:  case 0x12:  case 0x13:
    case 0x14:  case 0x15:  case 0x16:  case 0x17:
    case 0x18:  case 0x19:  case 0x1A:  case 0x1B:
    case 0x1C:  case 0x1D:  case 0x1E:  case 0x1F:
        LOG("unknown character " << c); //hex8 not defined
        return;
    }

    if (this->cursorx >= ncolumns) {
        this->LineFeed();
        this->cursorx = 0;
    }

    auto cx = this->cursorx;
    auto cy = this->cursory;
    this->screen[cy][cx] = c;
    //DebugMessage("Write: " + String.fromCharCode(c));
    this->color[cy][cx] = this->currentcolor;
    this->cursorx++;
    this->PlotRow(this->cursory);
};

void BetterTerminal::update() {
    mutex.lock();
    std::queue<char> empty;
    std::swap( queue, empty );
    mutex.unlock();

    if(blinking.getElapsedTime().asSeconds() > 0.5) {
        Blink();
    }

    while(!empty.empty()) {
        ProcessChar(empty.front());
        empty.pop();
    }

    canvas.begin();


    sf::FloatRect dst(0,0,CharWidth,CharHeight);
    sf::IntRect src(0,0,CharWidth,CharHeight);
    sf::IntRect blank(21,10,1,1);

    for(int x=0; x<ncolumns; x++) {
        dst.top = 0;
        for(int y=0; y<nrows; y++) {
            auto ccolor = (uint)this->color[y][x];
            if (this->cursorvisible && cursory == y && cursorx == x) {
                ccolor |= 0x600;
            }
            sf::Color* bg = &Colors[(ccolor >> 8) & 0x1F];
            sf::Color* fg = &Colors[(ccolor) & 0x1F];

            char code = screen[y][x];

            src.left = (code % 16) * CharWidth;
            src.top = (code / 16) * CharHeight;

            canvas.draw(&sheet,dst,blank,*bg);
            canvas.draw(&sheet,dst,src,*fg);

            dst.top += CharHeight;
        }
        dst.left += CharWidth;
    }

    canvas.end();
}

void BetterTerminal::keyPressed(sf::Keyboard::Key code, bool ctrl)
{
    if(ctrl) {
        char c = getUnicode(code);
        if(c != '\0') {
            uartdev->ReceiveChar(c & 0x1F);
        }
    }

    if(code == sf::Keyboard::Up) uartdev->ReceiveChar(0x10);
    if(code == sf::Keyboard::Down) uartdev->ReceiveChar(0x0E);
    if(code == sf::Keyboard::Left) uartdev->ReceiveChar(0x2);
    if(code == sf::Keyboard::Right) uartdev->ReceiveChar(0x6);
}

void BetterTerminal::textEntered(char unicode)
{
    if(unicode == '\n') unicode = 13;

    uartdev->ReceiveChar(unicode);
}

Tile *BetterTerminal::at(int x, int y)
{
    return &tiles[x+y*TerminalColumns];
}

void BetterTerminal::PutChar(char x)
{
    mutex.lock();
    queue.push(x);
    mutex.unlock();
}
