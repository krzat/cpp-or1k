#ifndef BETTERTERMINAL_H
#define BETTERTERMINAL_H

#include "ORK/uartdevice.h"
#include "pch.h"
#include "spritebatch.h"

#include <SFML/Graphics/Drawable.hpp>


struct Tile {
    char code;
    sf::Color *bg;
    sf::Color *fg;
};


class BetterTerminal : public ITerminal, public sf::Drawable
{
public:
    BetterTerminal(UARTDevice& uart);

    // ITerminal interface
public:
    void PutChar(char x);
    void update();
    void keyPressed(sf::Keyboard::Key key, bool ctrl);
    void textEntered(char unicode);

private:
    UARTDevice* uartdev;

    bool cursorvisible;
    int escapetype;
    std::string escapestring;
    int cursorx;
    int cursory;
    int scrolltop;
    int scrollbottom;
    int currentcolor;

    std::queue<char> queue;
    std::mutex mutex;

    SpriteBatch canvas;

    sf::Clock blinking;

    std::vector<Tile> tiles;
    Tile* at(int x, int y);

    std::vector< std::vector<int> > screen;
    std::vector< std::vector<int> > color;
    void Blink();
    void DeleteRow(int row);
    void DeleteArea(int row, int column, int row2, int column2);
    void PlotRow(int row);
    void ScreenUpdate();
    void ScrollDown();
    void ScrollDown2(bool draw=false);
    void ScrollUp(bool draw);
    void LineFeed();
    void Scroll(int count);
    void ChangeCursor(std::vector<int> &Numbers);
    void ChangeColor(std::vector<int> &Numbers);
    void HandleEscapeSequence();

    // Drawable interface
    void ProcessChar(char c);
protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;
};

#endif // BETTERTERMINAL_H
