# cpp-or1k

![cpp-or1k.png](https://bitbucket.org/repo/XjAoab/images/948225060-cpp-or1k.png)

C++ version of jor1k emulator with SFML used for interface.

Not implemented: networking, frame buffer.

Additional instructions:

Use QtCreator to compile or do it manually. sfml libraries are the only dependency.

-----


# jor1k

jor1k is a OpenRISC 1000 emulator written in JavaScript running Linux. It runs in almost any modern web browser. 
Have a try and see if it runs in your browser by opening the [demo][project demo].
More information can be found on the [Wiki][project wiki] pages. 

### Links

 * A project [demo][project demo] is available
 * [Bugtracker][project issues] to report any issues or feature requests
 * [Wiki][project wiki] containing more detailed descriptions

### LICENSE
The program is distributed under the terms of the GNU General Public
License.

### Developer
Sebastian Macke [simulationcorner.net](http://simulationcorner.net)

### Contributors
Gerard Braad [gbraad.nl](http://gbraad.nl)  
Ben Burns [github.com/benjamincburns](http://github.com/benjamincburns)


[or1k specification]: http://opencores.org/or1k/Main_Page
[project demo]: http://s-macke.github.com/jor1k/
[project issues]: https://github.com/s-macke/jor1k/issues
[project wiki]: https://github.com/s-macke/jor1k/wiki