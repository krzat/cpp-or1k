#ifndef PCH_H
#define PCH_H

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

typedef sf::Uint8 byte;
typedef unsigned int uint;
typedef unsigned short ushort;

typedef std::vector<byte> Data;

//class Device;
//typedef std::vector<std::unique_ptr<Device>> Devices;

#ifdef DEBUG
#define LOG(x) (std::cout << x << std::endl)
#else
#define LOG(x)
#endif

#define UNUSED(x) (void)x;

const int CharWidth = 8;
const int CharHeight = 16;
const int TerminalColumns = 80;
const int TerminalRows = 24;
const int ScreenWidth = TerminalColumns * CharWidth;
const int ScreenHeight = TerminalRows * CharHeight;


#endif // PCH_H
