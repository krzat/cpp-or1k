#-------------------------------------------------
#
# Project created by QtCreator 2014-06-20T10:28:59
#
#-------------------------------------------------

TARGET = test
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11
CONFIG -= qt

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE *= -O3

TEMPLATE = app

SOURCES += main.cpp \
    ORK/atadevice.cpp \
    ORK/cpu.cpp \
    ORK/keyboarddevice.cpp \
    ORK/machine.cpp \
    ORK/ram.cpp \
    ORK/uartdevice.cpp \
    ORK/device.cpp \
    betterterminal.cpp \
    spritebatch.cpp


LIBS += -L"/usr/local/lib"

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-system -lsfml-network -lsfml-window
CONFIG(debug, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-system -lsfml-network -lsfml-window

CONFIG(debug, debug|release): DEFINES += DEBUG

INCLUDEPATH += "/usr/local/include"
DEPENDPATH += "/usr/local/include"

OTHER_FILES += \
    convert.sh \
    hdgcc \
    vmlinux.bin \
    8x16.png \
    LICENSE.md \
    README.md

HEADERS += \
    ORK/atadevice.h \
    ORK/cpu.h \
    ORK/keyboarddevice.h \
    ORK/machine.h \
    ORK/ram.h \
    ORK/uartdevice.h \
    ORK/device.h \
    pch.h \
    betterterminal.h \
    spritebatch.h

