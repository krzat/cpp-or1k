#include <stdio.h>
#include "pch.h"
#include "ORK/machine.h"
#include <fstream>
#include "betterterminal.h"

/// Loads data into byte vector.
void load(std::string file, Data &output) {
    std::ifstream data (file, std::ios::binary | std::ios::ate);
    int size = data.tellg();
    output.resize(size);

    data.seekg(0, std::ios::beg);
    data.read((char*)&output[0], size);

}

/// Measures fps and returns current value.
float fps() {
    const int BufferSize =  100;
    static float array[BufferSize];
    static int pos;
    static sf::Clock clock;

    array[pos] = clock.getElapsedTime().asSeconds();
    clock.restart();
    pos++;
    if(pos >= BufferSize) pos = 0;

    float sum = 0;
    for(int i=0; i<BufferSize; i++) {
        sum += array[i];
    }

    return sum > 0? BufferSize/sum : 0;
}



int main(int argc, char *argv[]) {
    UNUSED(argc);
    UNUSED(argv);

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(ScreenWidth,ScreenHeight), "Game", sf::Style::Titlebar | sf::Style::Close, settings);
    window.setVerticalSyncEnabled(true);
    window.clear();
    window.display();

    sf::Texture texture;
    texture.create(ScreenWidth, ScreenHeight);
    sf::Color colors[ScreenWidth*ScreenHeight];
    texture.update((byte*)colors);
    sf::Sprite sprite(texture);



    Machine machine;
    BetterTerminal terminal(machine.uartdev);

    Data data;
    load("vmlinux.bin", data);
    machine.loadKernel(data);
    load("hdgcc", data);
    machine.loadDisk(data);

    machine.start();

    sf::Clock fpsUpdate;

    while(window.isOpen()) {

        sf::Event event;
        while(window.pollEvent(event)) {
            if(event.type == sf::Event::Closed) {
                window.close();
            }

            if(event.type == sf::Event::TextEntered) {
                char code = event.text.unicode;
                terminal.textEntered(code);
            }

            if(event.type == sf::Event::KeyPressed) {
                bool ctrl = event.key.control;
                auto code = event.key.code;

                terminal.keyPressed(code,ctrl);
            }
        }

        window.clear();
        window.draw(sprite);

        terminal.update();
        window.draw(terminal);

        float frames = fps();
        if(fpsUpdate.getElapsedTime().asSeconds() > 1.0) {
            fpsUpdate.restart();
            window.setTitle("fps:"+std::to_string(frames) + " mips:" + std::to_string(machine.mips));
        }


        window.display();
    }

    return 0;
}
