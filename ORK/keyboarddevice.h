#ifndef KEYBOARDDEVICE_H
#define KEYBOARDDEVICE_H

#include "pch.h"
#include "device.h"
class Machine;

class KeyboardDevice : public Device
{
public:
    int key;
    KeyboardDevice(Machine &intdev);
    void Reset();
    byte ReadReg8(int addr);
    bool Visible;
    sf::Clock watch;
};

#endif // KEYBOARDDEVICE_H
