#ifndef RAM_H
#define RAM_H

#include "pch.h"
#include "device.h"

class RAM
{
public:
    RAM(int size);
    std::vector<Device*> devices;

    int ReadMemory32(int addr);
    short ReadMemory16(int addr);
    byte ReadMemory8(int addr);
    void WriteMemory32(int addr, int x);
    void WriteMemory16(int addr, short x);
    void WriteMemory8(int addr, byte x);

    static int Swap32(int x);
    static short Swap16(short x);

    int* int32mem;
    byte* uint8mem;

private:
    Data data;
    Device* findDevice(uint uaddr);
};

#endif // RAM_H
