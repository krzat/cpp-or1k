#ifndef CPU_H
#define CPU_H
#include "pch.h"

class RAM;

enum EXCEPT
{
    // exception types and addresses
    ITLBMISS = 0xA00, // instruction translation lookaside buffer miss
    IPF = 0x400, // instruction page fault
    RESET = 0x100, // reset the processor
    DTLBMISS = 0x900, // data translation lookaside buffer miss
    DPF = 0x300, // instruction page fault
    BUSERR = 0x200, // wrong memory access
    TICK = 0x500, // tick counter interrupt
    INT = 0x800, // interrupt of external devices
    SYSCALL = 0xc00, // syscall, jump into supervisor mode
};

enum SPR {
    UPR = 1, // unit present register
    SR = 17, // supervision register
    EEAR_BASE = 48, // exception ea register
    EPCR_BASE = 32, // exception pc register
    ESR_BASE = 64, // exception sr register
    IMMUCFGR = 4, // Instruction MMU Configuration register
    DMMUCFGR = 3, // Data MMU Configuration register
    ICCFGR = 6, // Instruction Cache configuration register
    DCCFGR = 5, // Data Cache Configuration register
    VR = 0 // Version register
};

class CPU
{
public:
    CPU(RAM &ram);
    void Reset();
    void AnalyzeImage();
    void SetFlags(int x);
    int GetFlags();
    void RaiseInterrupt(int line);
    void ClearInterrupt(int line);
    void Step(int steps, int clockspeed);

    void updateTimer(int clockspeed);
private:
    void CheckForInterrupt();
    void InvalidateTLB();
    void SetSPR(int idx, int x);
    int GetSPR(int idx);
    void Exception(EXCEPT excepttype, int addr);
    bool DTLBRefill(int addr, int nsets);
    void abort();

    RAM* ram;
    bool SR_SM;
    bool SR_TEE;
    bool SR_IEE;
    bool SR_DCE;
    bool SR_ICE;
    bool SR_DME;
    bool SR_IME;
    bool SR_LEE;
    bool SR_CE;
    bool SR_F;
    bool SR_CY;
    bool SR_OV;
    bool SR_OVE;
    bool SR_DSX;
    bool SR_EPH;
    bool SR_FO;
    bool SR_SUMRA;
    int SR_CID;

    int fasttlblookup[6<<2], fasttlbcheck[6<<2];

    int PICSR;
    int pc;
    int nextpc;
    bool delayedins;
    bool interrupt_pending;
    int group2[1024];
    int group1[1024];
    int group0[1024];
    int* r;
    float* f;
    uint* ur;

    int registers[34];

    int TTMR;
    int TTCR;
    int PICMR;
    int boot_dtlb_misshandler_address;
    int boot_itlb_misshandler_address;
    int current_pgd;


    int GetInstruction(int addr);
    int DTLBLookup(int addr, bool write);
    bool ITLBRefill(int addr, int nsets);
    void Step();
};

#endif // CPU_H
