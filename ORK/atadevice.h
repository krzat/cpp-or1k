#ifndef ATADEVICE_H
#define ATADEVICE_H

#include "pch.h"
#include "device.h"
class Machine;


class ATADevice : public Device
{
public:
    ATADevice(Machine &intdev);

    void setBuffer(Data &data);
    void Reset();
    void SetBuffer(Data &buffer);
    void BuildIdentifyBuffer(ushort *buffer16);
    int GetSector();
    void SetSector(int sector);
    void ExecuteCommand();
    byte ReadReg8(int addr);
    void WriteReg8(int addr, byte x);
    short ReadReg16(int addr);
    void WriteReg16(int addr, short x);
    int ReadReg32(int addr);
    void WriteReg32(int addr, int x);
private:
    int CR;
    byte DCR;
    int DR;
    int SCR;
    int SNR;
    int SR;
    int FR;
    int ER;
    int lcyl;
    int hcyl;
    int select;
    bool driveselected;
    int readbufferindex;
    int readbuffermax;
    short cylinders;
    short heads;
    short sectors;
    int nsectors;

    std::vector<ushort> diskbuffer;

    ushort identifybuffer[256];
    ushort *readbuffer;
};

#endif // ATADEVICE_H
