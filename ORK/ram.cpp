#include "ram.h"

RAM::RAM(int size)
{
    data.resize(size);
    this->int32mem = (int*)&data[0];
    this->uint8mem = &data[0];
}

Device* RAM::findDevice(uint uaddr) {
    for(Device* device : devices) {
        if ((uaddr >= device->deviceaddr) && (uaddr < (device->deviceaddr+device->devicesize))) {
            return device;
        }
    }

    return NULL;
}

int RAM::Swap32(int val)
{
    //TODO: verify >>
    return ((val & 0xFF) << 24) | ((val & 0xFF00) << 8) | ((val >> 8) & 0xFF00) | ((val >> 24) & 0xFF);
}

short RAM::Swap16(short val)
{
    auto ext = (short)val; //TODO: check this
    return (short)(((ext & 0xFF) << 8) | ((ext >> 8) & 0xFF));
}

int RAM::ReadMemory32(int addr)
{
    if (addr >= 0) {
        return this->int32mem[addr >> 2];
    }

    auto uaddr = (uint)addr;
    Device* device = findDevice(uaddr);
    if(!device) return 0;
    return device->ReadReg32((int)(uaddr - device->deviceaddr));
}

void RAM::WriteMemory32(int addr, int x)
{
    if (addr >= 0) {
        this->int32mem[addr >> 2] = x;
        return;
    }

    auto uaddr = (uint)addr;
    Device* device = findDevice(uaddr);
    if(!device) return;
    return device->WriteReg32((int)(uaddr - device->deviceaddr), x);
}

byte RAM::ReadMemory8(int addr)
{
    if (addr >= 0) {
        return this->uint8mem[addr ^ 3];
    }
    auto uaddr = (uint)addr;
    Device* device = findDevice(uaddr);
    if(!device) return 0;
    return device->ReadReg8((int)(uaddr - device->deviceaddr));
}

void RAM::WriteMemory8(int addr, byte x)
{
    if (addr >= 0) {
        this->uint8mem[addr ^ 3] = x;
        return;
    }
    auto uaddr = (uint)addr;
    Device* device = findDevice(uaddr);
    if(!device) return;
    return device->WriteReg8((int)(uaddr - device->deviceaddr), x);
}

short RAM::ReadMemory16(int addr)
{
    if (addr >= 0) {
        return (short)(      (this->uint8mem[(addr ^ 2)+1] << 8) | this->uint8mem[(addr ^ 2)]);
    }
    auto uaddr = (uint)addr;
    Device* device = findDevice(uaddr);
    if(!device) return 0;
    return device->ReadReg16((int)(uaddr - device->deviceaddr));
}

void RAM::WriteMemory16(int addr, short x)
{
    if (addr >= 0) {
        this->uint8mem[(addr ^ 2)+1] = (byte)((x >> 8) & 0xFF);
        this->uint8mem[(addr ^ 2)  ] = (byte)(x & 0xFF);
        return;
    }
    auto uaddr = (uint)addr;
    Device* device = findDevice(uaddr);
    if(!device) return;
    return device->WriteReg16((int)(uaddr - device->deviceaddr), x);

}


