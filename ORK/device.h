#ifndef DEVICE_H
#define DEVICE_H
#include "pch.h"


class Machine;

class Device
{
public:
    Device();

    Machine* intdev;
    uint deviceaddr;
    uint devicesize;

    Device(Machine &machine)
    {
        intdev = &machine;
    }

    virtual void WriteReg16(int addr, short x)
    {
        (void)addr;
        (void)x;
    }
    virtual short ReadReg16(int addr)
    {
        (void)addr;
        return 0;
    }
    virtual void WriteReg32(int addr, int x)
    {
        (void)addr;
        (void)x;
    }
    virtual int ReadReg32(int addr)
    {
        (void)addr;
        return 0;
    }
    virtual void WriteReg8(int addr, byte x)
    {
        (void)addr;
        (void)x;
    }
    virtual byte ReadReg8(int addr)
    {
        (void)addr;
        return 0;
    }

    virtual void Reset()
    {
    }

};

#endif // DEVICE_H
