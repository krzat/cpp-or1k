#ifndef UARTDEVICE_H
#define UARTDEVICE_H

#include <queue>
#include "pch.h"
#include "device.h"
class Machine;

class ITerminal
{
public:
    virtual void PutChar(char x) = 0;
};

class UARTDevice : public Device
{
public:
    UARTDevice(Machine &intdev);

    void Reset();
    void ReceiveChar(char x);
    void ThrowCTI();
    void ThrowTHRI();
    void NextInterrupt();
    void ClearInterrupt(int line);
    byte ReadReg8(int addr);
    void WriteReg8(int addr, byte x);

    ITerminal *Output;
private:
    int LSR;
    int input;
    int LCR;
    int MSR;
    int ints;
    int IER;
    int DLL;
    int DLH;
    int FCR;
    int MCR;

    std::queue<int> fifo;
    int IIR;
};

#endif // UARTDEVICE_H
