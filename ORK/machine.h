#ifndef MACHINE_H
#define MACHINE_H

#include <thread>
#include <atomic>

#include "pch.h"
#include "cpu.h"
#include "ram.h"
#include "device.h"
#include "atadevice.h"
#include "keyboarddevice.h"
#include "uartdevice.h"

enum MachineState {
    Stopped, //default
    Starting,
    Running,
    Stopping,
};

class Machine
{

private:
    RAM ram;
    CPU cpu;

    std::thread worker;
    std::atomic<MachineState> state;

    void run();

public:
    Machine();
    ~Machine();

    void start();
    void stop();
    void reset();
    void loadKernel(Data &data);
    void loadDisk(Data &data);

    bool getState();

    void RaiseInterrupt(int page);
    void ClearInterrupt(int page);

    ATADevice atadev;
    UARTDevice uartdev;
    KeyboardDevice keyboarddev;
    float mips;
};

#endif // MACHINE_H
