#include "atadevice.h"
#include "machine.h"
#include "ram.h"

enum ATA
    {
        // ATA command block registers
        // 2 is the reg_shift
    REG_DATA = 0x00 << 2, // data register
    REG_ERR = 0x01 << 2, // error register, feature register
    REG_NSECT = 0x02 << 2, // sector count register
    REG_LBAL = 0x03 << 2, // sector number register
    REG_LBAM = 0x04 << 2, // cylinder low register
    REG_LBAH = 0x05 << 2, // cylinder high register
    REG_DEVICE = 0x06 << 2, // drive/head register
    REG_STATUS = 0x07 << 2, // status register // command register

    REG_FEATURE = REG_ERR, // and their aliases (writing)
    REG_CMD = REG_STATUS,
    REG_BYTEL = REG_LBAM,
    REG_BYTEH = REG_LBAH,
    REG_DEVSEL = REG_DEVICE,
    REG_IRQ = REG_NSECT,

    // device control register
    DCR_RST = 0x04,	// Software reset   (RST=1, reset)
    DCR_IEN = 0x02,	// Interrupt Enable (IEN=0, enabled)

    // ----- ATA (Alternate) Status Register
    SR_BSY = 0x80,  // Busy
    SR_DRDY = 0x40,  // Device Ready
    SR_DF = 0x20,  // Device Fault
    SR_DSC = 0x10,  // Device Seek Complete
    SR_DRQ = 0x08,  // Data Request
    SR_COR = 0x04,  // Corrected data (obsolete)
    SR_IDX = 0x02,  //                (obsolete)
    SR_ERR = 0x01  // Error
};



// constructor
ATADevice::ATADevice(Machine& intdev) : Device(intdev)
{
    this->Reset();

    Data buffer;
    buffer.resize(1024*1024);
    this->SetBuffer(buffer);

}

void ATADevice::Reset()
{
    this->DCR = 0x8; // fourth bis is always set
    this->DR = 0xA0; // some bits are always set to one
    this->SCR = 0x1;
    this->SNR = 0x1;
    this->SR = ATA::SR_DRDY; // status register
    this->FR = 0x0; // Feature register
    this->ER = 0x1; // Error register
    this->CR = 0x0; // Command register

    //this->error = 0x1;
    this->lcyl = 0x0;
    this->hcyl = 0x0;
    this->select = 0xA0;
    this->driveselected = true; // drive no 0

    this->readbuffer = this->identifybuffer;
    this->readbufferindex = 0;
    this->readbuffermax = 256;
}

void ATADevice::SetBuffer(Data &buffer)
{
    diskbuffer.resize(buffer.size()/2);
    memcpy(&diskbuffer[0], &buffer[0], buffer.size());


    this->heads = 16;
    this->sectors = 64;
    this->cylinders = (short)(buffer.size() / (this->heads * this->sectors * 512));
    this->nsectors = this->heads * this->sectors * this->cylinders;
    this->BuildIdentifyBuffer(this->identifybuffer);
}

void ATADevice::BuildIdentifyBuffer(ushort* buffer16)
{
    for (auto i = 0; i < 256; i++)
    {
        buffer16[i] = 0x0000;
    }

    buffer16[0] = 0x0040;
    buffer16[1] = (ushort)this->cylinders; // cylinders
    buffer16[3] = (ushort)this->heads; // heads
    buffer16[4] = (ushort)(512 * this->sectors); // Number of unformatted bytes per track (sectors*512)
    buffer16[5] = 512; // Number of unformatted bytes per sector
    buffer16[6] = (ushort)this->sectors; // sectors per track

    buffer16[20] = 0x0003; // buffer type
    buffer16[21] = 512; // buffer size in 512 bytes increment
    buffer16[22] = 4; // number of ECC bytes available

    buffer16[27] = 0x6A6F; // jo (model string)
    buffer16[28] = 0x7231; // r1
    buffer16[29] = 0x6B2D; // k-
    buffer16[30] = 0x6469; // di
    buffer16[31] = 0x736B; // sk
    for (auto i = 32; i <= 46; i++)
    {
        buffer16[i] = 0x2020; // (model string)
    }

    buffer16[47] = (ushort)(0x8000 | 128);
    buffer16[48] = 0x0000;
    buffer16[49] = 1 << 9;
    buffer16[51] = 0x200; // PIO data transfer cycle timing mode
    buffer16[52] = 0x200; // DMA data transfer cycle timing mode

    buffer16[54] = (ushort)this->cylinders;
    buffer16[55] = (ushort)this->heads;
    buffer16[56] = (ushort)this->sectors; // sectors per track

    buffer16[57] = (ushort)((uint)(this->nsectors) & 0xFFFF); // number of sectors
    buffer16[58] = (ushort)((this->nsectors >> 16) & 0xFFFF);

    buffer16[59] = 0x0000; // multiple sector settings
    //buffer16[59]  = 0x100 | 128;

    buffer16[60] = (ushort)((uint)(this->nsectors) & 0xFFFF); // Total number of user-addressable sectors low
    buffer16[61] = (ushort)((this->nsectors >> 16) & 0xFFFF); // Total number of user-addressable sectors high

    buffer16[80] = (1 << 1) | (1 << 2); // version, support ATA-1 and ATA-2
    buffer16[82] = (1 << 14); // Command sets supported. (NOP supported)
    buffer16[83] = (1 << 14); // this->bit should be set to one
    buffer16[84] = (1 << 14); // this->bit should be set to one
    buffer16[85] = (1 << 14); // Command set/feature enabled (NOP)
    buffer16[86] = 0; // Command set/feature enabled
    buffer16[87] = (1 << 14); // Shall be set to one

}

int ATADevice::GetSector()
{
    if (0 == (this->DR & 0x40))
    {
        abort();
    }
    return ((this->DR & 0x0F) << 24) | (this->hcyl << 16) | (this->lcyl << 8) | this->SCR;
}

void ATADevice::SetSector(int sector)
{
    if (0 == (this->DR & 0x40))
    {
        abort();
    }
    this->SCR = sector & 0xFF;
    this->lcyl = (sector >> 8) & 0xFF;
    this->hcyl = (sector >> 16) & 0xFF;
    this->DR = (this->DR & 0xF0) | ((sector >> 24) & 0x0F);
}

void ATADevice::ExecuteCommand()
{
    switch (this->CR)
    {
        case 0xEC: // identify device
            this->readbuffer = this->identifybuffer;
            this->readbufferindex = 0;
            this->readbuffermax = 256;
            this->SR = ATA::SR_DRDY | ATA::SR_DSC | ATA::SR_DRQ;
            if (0 == (this->DCR & ATA::DCR_IEN))
            {
                this->intdev->RaiseInterrupt(15);
            }
            break;

        case 0x91: // initialize drive parameters
            this->SR = ATA::SR_DRDY | ATA::SR_DSC;
            this->ER = 0x0;
            if (0 == (this->DCR & ATA::DCR_IEN))
            {
                this->intdev->RaiseInterrupt(15);
            }
            break;

        case 0x20: // load sector
        case 0x30: {// save sector

            auto sector = this->GetSector();
            if (this->SNR == 0)
            {
                this->SNR = 256;
            }
            //DebugMessage("ATADev: Load sector " + hex8(sector) + ". number of sectors " + hex8(this->SNR));
            this->readbuffer = &this->diskbuffer[0];
            this->readbufferindex = sector * 256;
            this->readbuffermax = this->readbufferindex + 256;
            this->SR = ATA::SR_DRDY | ATA::SR_DSC | ATA::SR_DRQ;
            this->ER = 0x0;
            if (this->CR == 0x20)
            {
                if (0 == (this->DCR & ATA::DCR_IEN))
                {
                    this->intdev->RaiseInterrupt(15);
                }
            }}
            break;

        case 0xC4: {}// read multiple sectors
        case 0xC5: {// write multiple sectors
            auto _sector = this->GetSector();
            if (this->SNR == 0)
            {
                this->SNR = 256;
            }
            //DebugMessage("ATADev: Load multiple sector " + hex8(sector) + ". number of sectors " + hex8(this->SNR));
            this->readbuffer = &this->diskbuffer[0];
            this->readbufferindex = _sector * 256;
            this->readbuffermax = this->readbufferindex + 256 * this->SNR;
            this->SR = ATA::SR_DRDY | ATA::SR_DSC | ATA::SR_DRQ;
            this->ER = 0x0;
            if (this->CR == 0xC4)
            {
                if (0 == (this->DCR & ATA::DCR_IEN))
                {
                    this->intdev->RaiseInterrupt(15);
                }
            }}

            break;

        default:
            abort();
            break;
    }
}

byte ATADevice::ReadReg8(int addr)
{

    if (!this->driveselected)
    {
        return 0xFF;
    }
    switch (addr)
    {
        case ATA::REG_ERR:
            //DebugMessage("ATADev: read error register");
            return (byte)this->ER;

        case ATA::REG_NSECT:
            //DebugMessage("ATADev: read sector count register");
            return (byte)this->SNR;

        case ATA::REG_LBAL:
            //DebugMessage("ATADev: read sector number register");
            return (byte)this->SCR;

        case ATA::REG_LBAM:
            //DebugMessage("ATADev: read cylinder low register");
            return (byte)this->lcyl;

        case ATA::REG_LBAH:
            //DebugMessage("ATADev: read cylinder high register");
            return (byte)this->hcyl;

        case ATA::REG_DEVICE:
            //DebugMessage("ATADev: read drive/head register");
            return (byte)this->DR;

        case ATA::REG_STATUS:
            //DebugMessage("ATADev: read status register");
            this->intdev->ClearInterrupt(15);
            return (byte)this->SR;

        case 0x100: // device control register, but read as status register
            //DebugMessage("ATADev: read alternate status register")
            return (byte)this->SR;
        //break;

        default:
            abort();
            break;
    }
    return 0x0;
}

void ATADevice::WriteReg8(int addr, byte x)
{

    if (addr == ATA::REG_DEVICE)
    {
        //DebugMessage("ATADev: Write drive/head register value: " + hex8(x));
        this->DR = x;
        //DebugMessage("Head " + (x&0xF));
        //DebugMessage("Drive No. " + ((x>>4)&1));
        //DebugMessage("LBA Mode " + ((x>>6)&1));
        this->driveselected = ((x >> 4) & 1) == 0;
        return;
    }

    if (addr == 0x100)
    { //device control register
        //DebugMessage("ATADev: Write CTL register" + " value: " + hex8(x));

        if (0 == (x & ATA::DCR_RST) && 0 != (this->DCR & ATA::DCR_RST))
        { // reset done
            //DebugMessage("ATADev: drive reset done");
            this->DR &= 0xF0; // reset head
            this->SR = ATA::SR_DRDY | ATA::SR_DSC;
            this->SCR = 0x1;
            this->SNR = 0x1;
            this->lcyl = 0x0;
            this->hcyl = 0x0;
            this->ER = 0x1;
            this->CR = 0x0;
        }
        else
            if (0 != (x & ATA::DCR_RST) && 0 == (this->DCR & ATA::DCR_RST))
            { // reset
                //DebugMessage("ATADev: drive reset");
                this->ER = 0x1; // set diagnostics message
                this->SR = ATA::SR_BSY | ATA::SR_DSC;
            }

        this->DCR = x;
        return;
    }

    if (!this->driveselected)
    {
        return;
    }

    switch (addr)
    {
        case ATA::REG_FEATURE:
            //DebugMessage("ATADev: Write feature register value: " + hex8(x));
            this->FR = x;
            break;

        case ATA::REG_NSECT:
            //DebugMessage("ATADev: Write sector count register value: " + hex8(x));
            this->SNR = x;
            break;

        case ATA::REG_LBAL:
            //DebugMessage("ATADev: Write sector number register value: " + hex8(x));
            this->SCR = x;
            break;

        case ATA::REG_LBAM:
            //DebugMessage("ATADev: Write cylinder low register value: " + hex8(x));
            this->lcyl = x;
            break;

        case ATA::REG_LBAH:
            //DebugMessage("ATADev: Write cylinder high number register value: " + hex8(x));
            this->hcyl = x;
            break;

        case ATA::REG_CMD:
            //DebugMessage("ATADev: Write Command register " + hex8(x));
            this->CR = x;
            this->ExecuteCommand();
            break;

        default:
            abort();
            break;
    }
}

short ATADevice::ReadReg16(int addr)
{
    if (addr != 0)
    { // data register
        abort();
    }

    //auto readx = readbuffer[this->readbufferindex];

    auto val = RAM::Swap16((short)this->readbuffer[this->readbufferindex]);
    //DebugMessage("ATADev: read data register");
    this->readbufferindex++;
    if (this->readbufferindex >= this->readbuffermax)
    {
        this->SR = ATA::SR_DRDY | ATA::SR_DSC; // maybe no DSC for identify command but it works

        if ((this->CR == 0x20) && (this->SNR > 1))
        {
            this->SNR--;
            this->SetSector(this->GetSector() + 1);
            this->readbuffermax += 256;
            this->SR = ATA::SR_DRDY | ATA::SR_DSC | ATA::SR_DRQ;
            if (0 == (this->DCR & ATA::DCR_IEN))
            {
                this->intdev->RaiseInterrupt(15);
            }
        }

    }

    return (short)val;
}

void ATADevice::WriteReg16(int addr, short x)
{
    if (addr != 0)
    { // data register
        abort();
    }
    this->readbuffer[this->readbufferindex] = (ushort)RAM::Swap16(x);
    //DebugMessage("ATADev: write data register");
    this->readbufferindex++;
    if (this->readbufferindex >= this->readbuffermax)
    {
        this->SR = ATA::SR_DRDY | ATA::SR_DSC;
        if (0 == (this->DCR & ATA::DCR_IEN))
        {
            this->intdev->RaiseInterrupt(15);
        }
        if ((this->CR == 0x30) && (this->SNR > 1))
        {
            this->SNR--;
            this->SetSector(this->GetSector() + 1);
            this->readbuffermax += 256;
            this->SR = ATA::SR_DRDY | ATA::SR_DSC | ATA::SR_DRQ;
        }
    }
}

int ATADevice::ReadReg32(int addr)
{
    (void)addr;
    abort();
    return 0;
}

void ATADevice::WriteReg32(int addr, int x)
{
    (void)addr;
    (void)x;
    abort();
}
