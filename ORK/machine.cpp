#include "machine.h"

Machine::Machine() : ram(1024*1024*64), cpu(ram), worker(&Machine::run, this), atadev(*this), uartdev(*this), keyboarddev(*this)
{
//     this.ram.AddDevice(this.atadev, 0x9e000000, 0x1000);
//     this.ram.AddDevice(this.uartdev, 0x90000000, 0x7);
//     this.ram.AddDevice(this.ethdev, 0x92000000, 0x1000);
//     this.ram.AddDevice(this.fbdev, 0x91000000, 0x1000);
//     this.ram.AddDevice(this.tsdev, 0x93000000, 0x1000);
//     this.ram.AddDevice(this.kbddev, 0x94000000, 0x100);

    atadev.deviceaddr = 0x9e000000; atadev.devicesize = 0x1000;
    uartdev.deviceaddr = 0x90000000; uartdev.devicesize = 0x7;
    keyboarddev.deviceaddr = 0x94000000; keyboarddev.devicesize = 0x100;

    ram.devices.push_back(&uartdev);
    ram.devices.push_back(&atadev);
    ram.devices.push_back(&keyboarddev);
}

Machine::~Machine()
{
    stop();
}

void Machine::start()
{
    state = MachineState::Starting;
    while(state != MachineState::Running);
}

void Machine::stop() {
    state = MachineState::Stopping;
    worker.join();
}

void Machine::reset() {

}

void Machine::loadKernel(Data &data)
{
    bool pause = state == MachineState::Running;
    if(pause) stop();

    int* src = (int*)&data[0];
    int* dst = ram.int32mem;

    for(int i=0; i<(int)data.size()/4;i++) {
        int x = RAM::Swap32(src[i]);
        dst[i] = x;

    }

    cpu.Reset();
    cpu.AnalyzeImage();

    if(pause) start();
}

void Machine::loadDisk(Data &data)
{
    atadev.SetBuffer(data);
}

void Machine::RaiseInterrupt(int page)
{
    cpu.RaiseInterrupt(page);
}

void Machine::ClearInterrupt(int page)
{
    cpu.ClearInterrupt(page);
}

void Machine::run() {
    LOG("Machine started");

    sf::Clock clock;
    int speed = 100;

    while(state != MachineState::Stopping) {
        if(state != MachineState::Starting) continue;
        state = MachineState::Running;

        LOG("Machine running");
        while(state == MachineState::Running) {
            clock.restart();
            cpu.Step(1000*1000*10, speed);
            float seconds = clock.getElapsedTime().asSeconds();
            mips = 10 / seconds;
            speed = (int)(mips / 10);
        }
        LOG("Machine stopping");
    }
    state = MachineState::Stopped;

    LOG("Machine stopped");
}
