#include "uartdevice.h"
#include "machine.h"

enum UART
{
    LSR_DATA_READY = 0x1,
    LSR_FIFO_EMPTY = 0x20,
    LSR_TRANSMITTER_EMPTY = 0x40,

    IER_THRI = 0x02, /* Enable Transmitter holding register int. */
    IER_RDI = 0x01, /* Enable receiver data interrupt */

    IIR_MSI = 0x00, /* Modem status interrupt (Low priority) */
    IIR_NO_INT = 0x01,
    IIR_THRI = 0x02, /* Transmitter holding register empty */
    IIR_RDI = 0x04, /* Receiver data interrupt */
    IIR_RLSI = 0x06, /* Receiver line status interrupt (High p.) */
    IIR_CTI = 0x0c, /* Character timeout */

    LCR_DLAB = 0x80, /* Divisor latch access bit */

    DLL = 0, /* R/W: Divisor Latch Low, DLAB=1 */
    DLH = 1, /* R/W: Divisor Latch High, DLAB=1 */

    IER = 1, /* R/W: Interrupt Enable Register */
    IIR = 2, /* R: Interrupt ID Register */
    FCR = 2, /* W: FIFO Control Register */
    LCR = 3, /* R/W: Line Control Register */
    MCR = 4, /* W: Modem Control Register */
    LSR = 5, /* R: Line Status Register */
    MSR = 6/* R: Modem Status Register */
};


// constructor
UARTDevice::UARTDevice(Machine &intdev) : Device(intdev)
{
    this->Reset();
}
void UARTDevice::Reset()
{
    this->LCR = 0x3; // Line Control, reset, character has 8 bits
    this->LSR = UART::LSR_TRANSMITTER_EMPTY | UART::LSR_FIFO_EMPTY; // Line Status register, Transmitter serial register empty and Transmitter buffer register empty
    this->MSR = 0; // modem status register
    this->IIR = UART::IIR_NO_INT; // // Interrupt Identification, no interrupt
    this->ints = 0x0; // no interrupt pending
    this->IER = 0x0; //Interrupt Enable
    this->DLL = 0;
    this->DLH = 0;
    this->FCR = 0x0; // FIFO Control;
    this->MCR = 0x0; // Modem Control
    this->input = 0;
}

// this is maybe too simple. No buffer. The character may be overwritten
// if the code needs some cycles to process. So the FIFO is done by the operating system
void UARTDevice::ReceiveChar(char x)
{
    this->fifo.push(x);
    if (this->fifo.size() >= 1)
    {
        this->input = this->fifo.front();
        fifo.pop();
        this->ClearInterrupt(UART::IIR_CTI);
        this->LSR |= UART::LSR_DATA_READY;
        this->ThrowCTI();
    }
}

void UARTDevice::ThrowCTI()
{
    this->ints |= 1 << UART::IIR_CTI;
    if (0 == (this->IER & UART::IER_RDI))
    {
        return;
    }
    if ((this->IIR != UART::IIR_RLSI) && (this->IIR != UART::IIR_RDI))
    {
        this->IIR = UART::IIR_CTI;
        this->intdev->RaiseInterrupt(0x2);
    }
}

void UARTDevice::ThrowTHRI()
{
    this->ints |= 1 << UART::IIR_THRI;
    if (0 == (this->IER & UART::IER_THRI))
    {
        return;
    }
    if (0 != (this->IIR & UART::IIR_NO_INT) || (this->IIR == UART::IIR_MSI) || (this->IIR == UART::IIR_THRI))
    {
        this->IIR = UART::IIR_THRI;
        this->intdev->RaiseInterrupt(0x2);
    }
}

void UARTDevice::NextInterrupt()
{
    if (0 != (this->ints & (1 << UART::IIR_CTI)) && 0 != (this->IER & UART::IER_RDI))
    {
        this->ThrowCTI();
    }
    else if (0 != (this->ints & (1 << UART::IIR_THRI)) && 0 != (this->IER & UART::IER_THRI))
    {
        this->ThrowTHRI();
    }
    else
    {
        this->IIR = UART::IIR_NO_INT;
        this->intdev->ClearInterrupt(0x2);
    }
}

void UARTDevice::ClearInterrupt(int line)
{
    this->ints &= ~(1 << line);
    this->IIR = UART::IIR_NO_INT;
    if (line != this->IIR)
    {
        return;
    }
    this->NextInterrupt();
}

byte UARTDevice::ReadReg8(int addr)
{
    if (0 != (this->LCR & UART::LCR_DLAB))
    {
        switch (addr)
        {
            case UART::DLL:
                return (byte)this->DLL;
            case UART::DLH:
                return (byte)this->DLH;
        }
    }
    switch (addr)
    {
        case 0:
            {
                auto ret = this->input;
                this->input = 0;
                this->ClearInterrupt(UART::IIR_RDI);
                this->ClearInterrupt(UART::IIR_CTI);
                if (this->fifo.size() >= 1)
                {
                    // not sure if this is right, probably not
                    this->input = this->fifo.front(); fifo.pop();
                    this->LSR |= UART::LSR_DATA_READY;
                    //this->ThrowCTI();
                }
                else
                {
                    this->LSR &= ~UART::LSR_DATA_READY;
                }
                return (byte)ret;
            }
        case UART::IER:
            return (byte)(this->IER & 0x0F);
        case UART::MSR:
            return (byte)this->MSR;
        case UART::IIR:
            {
                auto ret = (this->IIR & 0x0f) | 0xC0; // the two top bits are always set
                if (this->IIR == UART::IIR_THRI)
                {
                    this->ClearInterrupt(UART::IIR_THRI);
                }
                return (byte)ret;

            }
        case UART::LCR:
            return (byte)this->LCR;
        case UART::LSR:
            return (byte)this->LSR;

        default:
            abort();
            break;
    }
    return 0;
}

void UARTDevice::WriteReg8(int addr, byte x)
{
    x &= 0xFF;
    if (0 != (this->LCR & UART::LCR_DLAB))
    {
        switch (addr)
        {
            case UART::DLL:
                this->DLL = x;
                return;

            case UART::DLH:
                this->DLH = x;
                return;

        }
    }

    switch (addr)
    {
        case 0:
            this->LSR &= ~UART::LSR_FIFO_EMPTY;
            this->Output->PutChar((char)x);
            // Data is send with a latency of zero!
            this->LSR |= UART::LSR_FIFO_EMPTY; // send buffer is empty
            this->ThrowTHRI();
            break;
        case UART::IER:
            // 2 = 10b ,5=101b, 7=111b
            this->IER = x & 0x0F; // only the first four bits are valid
            // Ok, check immediately if there is a interrupt pending
            this->NextInterrupt();
            break;
        case UART::FCR:
            this->FCR = x;
            if (0 != (this->FCR & 2))
            {
                fifo = std::queue<int>();
            }
            break;
        case UART::LCR:
            this->LCR = x;
            break;
        case UART::MCR:
            this->MCR = x;
            break;
        default:
            abort();
            break;
    }
}
