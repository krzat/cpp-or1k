#include "cpu.h"
#include "ram.h"

void CPU::abort() {
    exit(0);
}

CPU::CPU(RAM &ram)
{
    this->ram = &ram;

    // registers
    // r[32] and r[33] are used to calculate the virtual address and physical address
    // to make sure that they are not transformed accidently into a floating point number

    this->r = (int*)registers;
    this->f = (float*)registers;
    this->ur = (uint*)registers;

    // define autoiables and initialize
    this->pc = 0x0; // instruction pointer in multiples of four
    this->nextpc = 0x0; // pointer to next instruction in multiples of four
    //this->ins=0x0; // current instruction to handle

    this->delayedins = false; // the current instruction is an delayed instruction, one cycle before a jump
    this->interrupt_pending = false;

    // fast tlb lookup for instruction and data
    // index 0: 32 bit instruction
    // index 1: data read 32 Bit
    // index 2: data read 8 Bit signed
    // index 3: data read 8 Bit unsigned
    // index 4: data write 32 Bit
    // index 5: data write 8 Bit

    this->InvalidateTLB();

    //this->clock = 0x0;

    this->TTMR = 0x0; // Tick timer mode register
    this->TTCR = 0x0; // Tick timer count register

    this->PICMR = 0x3; // interrupt controller mode register (use nmi)
    this->PICSR = 0x0; // interrupt controller set register

    // flags
    this->SR_SM = true; // supervisor mode
    this->SR_TEE = false; // tick timer Exception Enabled
    this->SR_IEE = false; // interrupt Exception Enabled
    this->SR_DCE = false; // Data Cache Enabled
    this->SR_ICE = false; // Instruction Cache Enabled
    this->SR_DME = false; // Data MMU Enabled
    this->SR_IME = false; // Instruction MMU Enabled
    this->SR_LEE = false; // Little Endian Enabled
    this->SR_CE = false; // CID Enabled ?
    this->SR_F = false; // Flag for l.sf... instructions
    this->SR_CY = false; // Carry Flag
    this->SR_OV = false; // Overflow Flag
    this->SR_OVE = false; // Overflow Flag Exception
    this->SR_DSX = false; // Delay Slot Exception
    this->SR_EPH = false; // Exception Prefix High
    this->SR_FO = true; // Fixed One, always set
    this->SR_SUMRA = false; // SPRS User Mode Read Access, or TRAP exception disable?
    this->SR_CID = 0x0; //Context ID

    this->Reset();
}

void CPU::Reset()
{
    this->TTMR = 0x0;
    this->TTCR = 0x0;
    this->PICMR = 0x3;
    this->PICSR = 0x0;

    this->group0[SPR::IMMUCFGR] = 0x18; // 0 ITLB has one way and 64 sets
    this->group0[SPR::DMMUCFGR] = 0x18; // 0 DTLB has one way and 64 sets
    this->Exception(EXCEPT::RESET, 0x0); // set pc values
    this->pc = this->nextpc;
    this->nextpc++;
}

void CPU::InvalidateTLB()
{
    this->fasttlblookup[0] = -1;
    this->fasttlblookup[1] = -1;
    this->fasttlblookup[2] = -1;
    this->fasttlblookup[3] = -1;
    this->fasttlblookup[4] = -1;
    this->fasttlblookup[5] = -1;
    this->fasttlbcheck[0] = -1;
    this->fasttlbcheck[1] = -1;
    this->fasttlbcheck[2] = -1;
    this->fasttlbcheck[3] = -1;
    this->fasttlbcheck[4] = -1;
    this->fasttlbcheck[5] = -1;
}


void CPU::AnalyzeImage() // get addresses for fast refill
{
    this->boot_dtlb_misshandler_address = this->ram->int32mem[0x900 >> 2];
    this->boot_itlb_misshandler_address = this->ram->int32mem[0xA00 >> 2];
    this->current_pgd = ((this->ram->int32mem[0x2018 >> 2] & 0xFFF) << 16) | (this->ram->int32mem[0x201C >> 2] & 0xFFFF);
}

void CPU::SetFlags(int x)
{
    this->SR_SM = (x & (1 << 0)) != 0;
    this->SR_TEE = (x & (1 << 1)) != 0;
    auto old_SR_IEE = this->SR_IEE;
    this->SR_IEE = (x & (1 << 2)) != 0;
    this->SR_DCE = (x & (1 << 3)) != 0;
    this->SR_ICE = (x & (1 << 4)) != 0;
    auto old_SR_DME = this->SR_DME;
    this->SR_DME = (x & (1 << 5)) != 0;
    auto old_SR_IME = this->SR_IME;
    this->SR_IME = (x & (1 << 6)) != 0;
    this->SR_LEE = (x & (1 << 7)) != 0;
    this->SR_CE = (x & (1 << 8)) != 0;
    this->SR_F = (x & (1 << 9)) != 0;
    this->SR_CY = (x & (1 << 10)) != 0;
    this->SR_OV = (x & (1 << 11)) != 0;
    this->SR_OVE = (x & (1 << 12)) != 0;
    this->SR_DSX = (x & (1 << 13)) != 0;
    this->SR_EPH = (x & (1 << 14)) != 0;
    this->SR_FO = true;
    this->SR_SUMRA = (x & (1 << 16)) != 0;
    this->SR_CID = (x >> 28) & 0xF;
    if (this->SR_LEE)
    {
        LOG("little endian not supported");
        abort();
    }
    if (this->SR_CID != 0)
    {
        LOG("context id not supported");
        abort();
    }
    if (this->SR_EPH)
    {
        LOG("exception prefix not supported");
        abort();
    }
    if (this->SR_DSX)
    {
        LOG("delay slot exception not supported");
        abort();
    }
    if (this->SR_IEE && !old_SR_IEE)
    {
        this->CheckForInterrupt();
    }
    if (!this->SR_IME && old_SR_IME)
    {
        this->fasttlblookup[0] = 0x0;
        this->fasttlbcheck[0] = 0x0;
    }
    if (!this->SR_DME && old_SR_DME)
    {
        this->fasttlblookup[1] = 0x0;
        this->fasttlbcheck[1] = 0x0;
        this->fasttlblookup[2] = 0x0;
        this->fasttlbcheck[2] = 0x0;
        this->fasttlblookup[3] = 0x0;
        this->fasttlbcheck[3] = 0x0;
        this->fasttlblookup[4] = 0x0;
        this->fasttlbcheck[4] = 0x0;
        this->fasttlblookup[5] = 0x0;
        this->fasttlbcheck[5] = 0x0;
    }
}

int CPU::GetFlags()
{
    auto x = 0x0;
    x |= this->SR_SM ? (1 << 0) : 0;
    x |= this->SR_TEE ? (1 << 1) : 0;
    x |= this->SR_IEE ? (1 << 2) : 0;
    x |= this->SR_DCE ? (1 << 3) : 0;
    x |= this->SR_ICE ? (1 << 4) : 0;
    x |= this->SR_DME ? (1 << 5) : 0;
    x |= this->SR_IME ? (1 << 6) : 0;
    x |= this->SR_LEE ? (1 << 7) : 0;
    x |= this->SR_CE ? (1 << 8) : 0;
    x |= this->SR_F ? (1 << 9) : 0;
    x |= this->SR_CY ? (1 << 10) : 0;
    x |= this->SR_OV ? (1 << 11) : 0;
    x |= this->SR_OVE ? (1 << 12) : 0;
    x |= this->SR_DSX ? (1 << 13) : 0;
    x |= this->SR_EPH ? (1 << 14) : 0;
    x |= this->SR_FO ? (1 << 15) : 0;
    x |= this->SR_SUMRA ? (1 << 16) : 0;
    x |= (this->SR_CID << 28);
    return x;
}

void CPU::CheckForInterrupt()
{
    if (!this->SR_IEE)
    {
        return;
    }
    if ((this->PICMR & this->PICSR) != 0)
    {
        this->interrupt_pending = true;
        /*
                // Do it here. Save one comparison in the main loop
                this->Exception(EXCEPT::INT, this->group0[SPR::EEAR_BASE]);
        */
    }
}

void CPU::RaiseInterrupt(int line)
{
    auto lmask = 1 << line;
    /*
        if (this->PICSR & lmask) {
            // Interrupt already signaled and pending
            // LOG("Warning: Int pending, ignored");
        }
    */
    this->PICSR |= lmask;
    this->CheckForInterrupt();
}

void CPU::ClearInterrupt(int line)
{
    this->PICSR &= ~(1 << line);
}

void CPU::SetSPR(int idx, int x)
{
    auto _idx = (int)idx;
    auto address = _idx & 0x7FF;
    auto group = (_idx >> 11) & 0x1F;

    switch (group)
    {
        case 1:
            // Data MMU
            this->group1[address] = x;
            return;
        case 2:
            // ins MMU

            this->group2[address] = x;
            return;
        case 3:
        // data cache, not supported
        case 4:
            // ins cache, not supported
            return;
        case 9:
            // pic
            switch (address)
            {
                case 0:
                    this->PICMR = x | 0x3; // we use non maskable interrupt here
                    // check immediate for interrupt
                    if (this->SR_IEE)
                    {
                        if (0 != (this->PICMR & this->PICSR))
                        {
                            LOG("Error in SetSPR: Direct triggering of interrupt exception not supported?");
                            abort();
                        }
                    }
                    break;
                case 2:
                    this->PICSR = x;
                    break;
                default:
                    LOG("Error in SetSPR: interrupt address not supported");
                    abort();
                    break;
            }
            return;
        case 10:
            //tick timer
            switch (address)
            {
                case 0:
                    this->TTMR = x;
                    if (((this->TTMR >> 30) & 3) != 0x3)
                    {
                        LOG("Error in SetSPR: Timer mode other than continuous not supported");
                        abort();
                    }
                    break;
                default:
                    LOG("Error in SetSPR: Tick timer address not supported");
                    abort();
                    break;
            }
            return;

        default:
            break;
    }

    if (group != 0)
    {
        LOG("Error in SetSPR: group " << group << " not found");
        abort();
    }

    switch (address)
    {
        case SPR::SR:
            this->SetFlags(x);
            break;
        case SPR::EEAR_BASE:
            this->group0[(int)SPR::EEAR_BASE] = x;
            break;
        case SPR::EPCR_BASE:
            this->group0[(int)SPR::EPCR_BASE] = x;
            break;
        case SPR::ESR_BASE:
            this->group0[(int)SPR::ESR_BASE] = x;
            break;
        default:
            LOG("Error in SetSPR: address " << address << " not found");
            abort();
            break;
    }
}

int CPU::GetSPR(int idx)
{
    auto _idx = (int)idx;
    auto address = _idx & 0x7FF;
    auto group = (_idx >> 11) & 0x1F;

    switch (group)
    {
        case 1:
            return this->group1[address];
        case 2:
            return this->group2[address];

        case 9:
            // pic
            switch (address)
            {
                case 0:
                    return this->PICMR;
                case 2:
                    return this->PICSR;
                default:
                    LOG("Error in GetSPR: PIC address unknown");
                    abort();
                    break;
            }
            break;

        case 10:
            // tick Timer
            switch (address)
            {
                case 0:
                    return this->TTMR;
                case 1:
                    return this->TTCR; // or clock
                default:
                    LOG("Error in GetSPR: Tick timer address unknown");
                    abort();
                    break;
            }
            break;
        default:
            break;
    }

    if (group != 0)
    {
        LOG("Error in GetSPR: group " << group << " unknown");
        abort();
    }

    switch (idx)
    {
        case SPR::SR:
            return this->GetFlags();

        case SPR::UPR:
            // UPR present
            // data mmu present
            // instruction mmu present
            // PIC present (architecture manual seems to be wrong here)
            // Tick timer present
            return 0x619;

        case SPR::IMMUCFGR:
        case SPR::DMMUCFGR:
        case SPR::EEAR_BASE:
        case SPR::EPCR_BASE:
        case SPR::ESR_BASE:
            return this->group0[idx];
        case SPR::ICCFGR:
            return 0x48;
        case SPR::DCCFGR:
            return 0x48;
        case SPR::VR:
            return 0x12000001;
        default:
            LOG("Error in GetSPR: address unknown");
            abort();
            break;
    }
    return 0;
}

void CPU::Exception(EXCEPT excepttype, int addr)
{
    int EXCEPT_vector = (int)((uint)excepttype | (this->SR_EPH ? 0xf0000000 : 0x0));
    //DebugMessage("Info: Raising Exception " + hex8(excepttype));

    this->SetSPR(SPR::EEAR_BASE, addr);
    this->SetSPR(SPR::ESR_BASE, this->GetFlags());

    this->SR_OVE = false;
    this->SR_SM = true;
    this->SR_IEE = false;
    this->SR_TEE = false;
    this->SR_DME = false;

    this->fasttlblookup[0] = 0x0;
    this->fasttlblookup[1] = 0x0;
    this->fasttlblookup[2] = 0x0;
    this->fasttlblookup[3] = 0x0;
    this->fasttlblookup[4] = 0x0;
    this->fasttlblookup[5] = 0x0;
    this->fasttlbcheck[0] = 0x0;
    this->fasttlbcheck[1] = 0x0;
    this->fasttlbcheck[2] = 0x0;
    this->fasttlbcheck[3] = 0x0;
    this->fasttlbcheck[4] = 0x0;
    this->fasttlbcheck[5] = 0x0;

    this->nextpc = EXCEPT_vector >> 2;

    switch (excepttype)
    {
        case EXCEPT::RESET:
            break;

        case EXCEPT::ITLBMISS:
        case EXCEPT::IPF:
            this->SetSPR(SPR::EPCR_BASE, addr - (this->delayedins ? 4 : 0));
            break;
        case EXCEPT::DTLBMISS:
        case EXCEPT::DPF:
        case EXCEPT::BUSERR:
            this->SetSPR(SPR::EPCR_BASE, (this->pc << 2) - (this->delayedins ? 4 : 0));
            break;

        case EXCEPT::TICK:
        case EXCEPT::INT:
            this->SetSPR(SPR::EPCR_BASE, (this->pc << 2) - (this->delayedins ? 4 : 0));
            break;
        case EXCEPT::SYSCALL:
            this->SetSPR(SPR::EPCR_BASE, (this->pc << 2) + 4 - (this->delayedins ? 4 : 0));
            break;
        default:
            LOG("Error in Exception: exception type not supported");
            abort();
            break;
    }
    this->delayedins = false;
    this->SR_IME = false;
}

// disassembled dtlb miss exception handler arch/openrisc/kernel/head.S, kernel dependent
bool CPU::DTLBRefill(int addr, int nsets)
{

    if (this->ram->int32mem[0x900 >> 2] == this->boot_dtlb_misshandler_address)
    {
        this->Exception(EXCEPT::DTLBMISS, addr);
        return false;
    }

    auto vpn = (addr >> 13) & 0x7FFFF;
    auto pgd_offset = ((addr >> 22) & 0x3FC);
    auto pte_offset = (vpn & 0x7FF) << 2;
    auto dmmucr = this->ram->int32mem[this->current_pgd >> 2] & 0x3FFFFFFF;
    auto pte_pointer = this->ram->int32mem[(dmmucr + pgd_offset) >> 2];
    if (0 ==pte_pointer)
    {
        this->Exception(EXCEPT::DPF, addr);
        return false;
    }
    auto pte = this->ram->int32mem[((pte_pointer & 0xffffe000) + pte_offset) >> 2];
    if (0 == (pte & 1))
    { // pte present
        this->Exception(EXCEPT::DPF, addr);
        return false;
    }
    auto idx = vpn & (nsets - 1);
    this->group1[0x280 | idx] = (int)(pte & (0xffffe000 | 0x3FA));
    this->group1[0x200 | idx] = (int)(addr & 0xffffe000) | 0x1;  // page_mask and valid bit
    return true;
}

// disassembled itlb miss exception handler arch/openrisc/kernel/head.S, kernel dependent
bool CPU::ITLBRefill(int addr, int nsets)
{

    if (this->ram->int32mem[0xA00 >> 2] == this->boot_itlb_misshandler_address)
    {
        this->Exception(EXCEPT::ITLBMISS, addr);
        return false;
    }

    auto vpn = (addr >> 13) & 0x7FFFF;
    auto pgd_offset = ((addr >> 22) & 0x3FC);
    auto pte_offset = (vpn & 0x7FF) << 2;
    auto dmmucr = this->ram->int32mem[this->current_pgd >> 2] & 0x3FFFFFFF;
    auto pte_pointer = this->ram->int32mem[(dmmucr + pgd_offset) >> 2];
    if (0 == pte_pointer)
    {
        this->Exception(EXCEPT::IPF, addr);
        return false;
    }
    auto pte = this->ram->int32mem[((pte_pointer & 0xffffe000) + pte_offset) >> 2];
    if (0 == (pte & 1))
    { // pte present
        this->Exception(EXCEPT::IPF, addr);
        return false;
    }
    auto idx = vpn & (nsets - 1);
    auto tr = pte & (0xffffe000 | 0x0FA);
    if (0 != (pte & 0x400))
    { // exec bit is set?
        tr |= 0xc0; // UXE and SXE bits
    }
    this->group2[0x280 | idx] = (int)tr;
    this->group2[0x200 | idx] = (int)(addr & 0xffffe000) | 0x1;  // page_mask and valid bit
    return true;
}

int CPU::DTLBLookup(int addr, bool write)
{
    if (!this->SR_DME)
    {
        return addr;
    }
    // pagesize is 8192 bytes
    // nways are 1
    // nsets are 64

    auto setindex = (addr >> 13) & 63; // check this values
    auto tlmbr = this->group1[0x200 | setindex]; // match register
    if (((tlmbr & 1) == 0) || ((tlmbr >> 19) != (addr >> 19)))
    {
        // use tlb refill to fasten up
        if (this->DTLBRefill(addr, 64))
        {
            tlmbr = this->group1[0x200 + setindex];
        }
        else
        {
            return -1;
        }
        // slow version
        // this->Exception(EXCEPT::DTLBMISS, addr);
        // return -1;
    }
    /* skipped this check
        // set lru
        if (tlmbr & 0xC0) {
            LOG("Error: LRU ist not supported");
            abort();
        }
    */
    auto tlbtr = this->group1[0x280 | setindex]; // translate register

    // check for page fault
    if (this->SR_SM)
    {
        if (
            ((!write) && (0 == (tlbtr & 0x100))) || // check if SRE
            ((write) && (0 == (tlbtr & 0x200)))     // check if SWE
           )
        {
            this->Exception(EXCEPT::DPF, addr);
            return -1;
        }
    }
    else
    {
        if (
               ((!write) && (0 == (tlbtr & 0x40))) || // check if URE
               ((write) && (0 == (tlbtr & 0x80)))     // check if UWE
           )
        {
            this->Exception(EXCEPT::DPF, addr);
            return -1;
        }
    }
    return (int)((tlbtr & 0xFFFFE000) | (uint)(addr & 0x1FFF));
}

// the slow and safe version
int CPU::GetInstruction(int addr) {
    if (!this->SR_IME)
    {
        return this->ram->ReadMemory32(addr);
    }
    // pagesize is 8192 bytes
    // nways are 1
    // nsets are 64

    auto setindex = (addr >> 13) & 63;
    setindex &= 63; // number of sets
    auto tlmbr = this->group2[0x200 | setindex];

    // test if tlmbr is valid
    if (((tlmbr & 1) == 0) || ((tlmbr >> 19) != (addr >> 19)))
    {
        this->Exception(EXCEPT::ITLBMISS, this->pc << 2);
        return -1;
    }
    // set lru
    if (0 != (tlmbr & 0xC0))
    {
        LOG("Error: LRU ist not supported");
        abort();
    }

    auto tlbtr = this->group2[0x280 | setindex];

    //Test for page fault
    if (this->SR_SM)
    {
        // check if user read enable is not set(URE)
        if (0 ==(tlbtr & 0x40))
        {
            this->Exception(EXCEPT::IPF, this->pc << 2);
            return -1;
        }
    }
    else
    {
        // check if supervisor read enable is not set (SRE)
        if (0 ==(tlbtr & 0x80))
        {
            this->Exception(EXCEPT::IPF, this->pc << 2);
            return -1;
        }
    }
    return this->ram->ReadMemory32((int)(tlbtr & 0xFFFFE000) | (addr & 0x1FFF));
}

void CPU::updateTimer(int clockspeed)
{
    if ((this->TTMR >> 30) != 0)
    {
        int delta = (this->TTMR & 0xFFFFFFF) - (this->TTCR & 0xFFFFFFF);
        delta += delta < 0 ? 0xFFFFFFF : 0x0;
        this->TTCR = (int)((this->TTCR + clockspeed) & 0xFFFFFFFF);
        if (delta < clockspeed)
        {
            // if interrupt enabled
            if (0 != (this->TTMR & (1 << 29)))
            {
                this->TTMR |= (1 << 28); // set pending interrupt
            }
        }
    }

    // check if pending and check if interrupt must be triggered
    if (this->SR_TEE && (this->TTMR & (1 << 28)) != 0)
    {
        this->Exception(EXCEPT::TICK, this->group0[(int)SPR::EEAR_BASE]);
        this->pc = this->nextpc++;
    }
    else
    {
        if (this->interrupt_pending)
        {
            // check again because there could be another exception during this one cycle
            if ((this->PICSR != 0) && (this->SR_IEE))
            {
                this->interrupt_pending = false;
                this->Exception(EXCEPT::INT, this->group0[(int)SPR::EEAR_BASE]);
                this->pc = this->nextpc++;
            }
        }
    }
}

void CPU::Step(int steps, int clockspeed)
{
    auto ins = 0x0;
    auto imm = 0x0;
    auto i = 0;
    auto rindex = 0x0;
    auto rA = 0x0;
    auto rB = 0x0;
    auto rD = 0x0;

    // local autoiables could be faster
    auto r = this->r;
    auto f = this->f;
    auto ftlb = this->fasttlblookup;
    auto ftlbcheck = this->fasttlbcheck;
    auto ram = this->ram;
    auto int32mem = this->ram->int32mem;
    auto group2 = this->group2;

    // to get the instruction
    auto setindex = 0x0;
    auto tlmbr = 0x0;
    auto tlbtr = 0x0;
    auto jump = 0x0;

    do
    {
        //this->clock++;

        // do this not so often
        if (0 == (steps & 63))
        {
            // ---------- TICK ----------
            // timer enabled
            updateTimer(clockspeed);
        }

        // Get Instruction Fast version
        if (0 != ((ftlbcheck[0] ^ this->pc) >> 11))
        { // short check if it is still the correct page
            ftlbcheck[0] = this->pc; // save the new page, lower 11 bits are ignored
            if (!this->SR_IME)
            {
                ftlb[0] = 0x0;
            }
            else
            {
                setindex = (this->pc >> 11) & 63; // check this values
                tlmbr = group2[0x200 | setindex];
                // test if tlmbr is valid
                if (
                    ((tlmbr & 1) == 0) || //test if valid
                    ((tlmbr >> 19) != (this->pc >> 17)))
                {
                    if (this->ITLBRefill(this->pc << 2, 64))
                    {
                        tlmbr = group2[0x200 | setindex]; // reload the new value
                    }
                    else
                    {
                        this->pc = this->nextpc++;
                        continue;
                    }
                }
                tlbtr = group2[0x280 | setindex]; //84806 wrong group, should be 396...
                //this->instlb = (tlbtr ^ tlmbr) & 0xFFFFE000;
                ftlb[0] = ((tlbtr ^ tlmbr) >> 13) << 11;
            }
        }
        ins = int32mem[(ftlb[0] ^ this->pc)];
        /*
        // for the slow autoiant
        ins = this->GetInstruction(this->pc<<2)
        if (ins == -1) {
            this->pc = this->nextpc++;
            continue;
        }
        */

        switch ((ins >> 26) & 0x3F)
        {
            case 0x0:
                // j
                jump = this->pc + ((ins << 6) >> 6);
                this->pc = this->nextpc;
                this->nextpc = jump;
                this->delayedins = true;
                continue;

            case 0x1:
                // jal
                jump = this->pc + ((ins << 6) >> 6);
                r[9] = (this->nextpc << 2) + 4;
                this->pc = this->nextpc;
                this->nextpc = jump;
                this->delayedins = true;
                continue;

            case 0x3:
                // bnf
                if (this->SR_F)
                {
                    break;
                }
                jump = this->pc + ((ins << 6) >> 6);
                this->pc = this->nextpc;
                this->nextpc = jump;
                this->delayedins = true;
                continue;
            case 0x4:
                // bf
                if (!this->SR_F)
                {
                    break;
                }
                jump = this->pc + ((ins << 6) >> 6);
                this->pc = this->nextpc;
                this->nextpc = jump;
                this->delayedins = true;
                continue;
            case 0x5:
                // nop
                break;
            case 0x6:
                // movhi or macrc
                rindex = (ins >> 21) & 0x1F;
                // if 16th bit is set
                if (0 != (ins & 0x10000))
                {
                    LOG("Error: macrc not supported\n");
                    abort();
                }
                else
                {
                    r[rindex] = ((ins & 0xFFFF) << 16); // movhi
                }
                break;

            case 0x8:
                //sys
                this->Exception(EXCEPT::SYSCALL, this->group0[SPR::EEAR_BASE]);
                break;

            case 0x9:
                // rfe
                this->nextpc = this->GetSPR(SPR::EPCR_BASE) >> 2;
                this->SetFlags(this->GetSPR(SPR::ESR_BASE));
                this->InvalidateTLB();
                break;

            case 0x11:
                // jr
                jump = r[(ins >> 11) & 0x1F] >> 2;
                this->pc = this->nextpc;
                this->nextpc = jump;
                this->delayedins = true;
                continue;
            case 0x12:
                // jalr
                jump = r[(ins >> 11) & 0x1F] >> 2;
                r[9] = (this->nextpc << 2) + 4;
                this->pc = this->nextpc;
                this->nextpc = jump;
                this->delayedins = true;
                continue;

            case 0x21:
                // lwz
                r[32] = r[(ins >> 16) & 0x1F] + ((ins << 16) >> 16);
                if ((r[32] & 3) != 0)
                {
                    LOG("Error in lwz: no unaligned access allowed");
                    abort();
                }
                if (0 != ((ftlbcheck[1] ^ r[32]) >> 13))
                {
                    r[33] = this->DTLBLookup(r[32], false);
                    if (r[33] == -1)
                    {
                        break;
                    }
                    ftlbcheck[1] = r[32];
                    ftlb[1] = ((r[33] ^ r[32]) >> 13) << 13;
                }
                r[33] = ftlb[1] ^ r[32];
                r[(ins >> 21) & 0x1F] = r[33] > 0 ? ram->int32mem[r[33] >> 2] : ram->ReadMemory32(r[33]);
                break;

            case 0x23:
                // lbz
                r[32] = r[(ins >> 16) & 0x1F] + ((ins << 16) >> 16);
                if (0 != ((ftlbcheck[2] ^ r[32]) >> 13))
                {
                    r[33] = this->DTLBLookup(r[32], false);
                    if (r[33] == -1)
                    {
                        break;
                    }
                    ftlbcheck[2] = r[32];
                    ftlb[2] = ((r[33] ^ r[32]) >> 13) << 13;
                }
                r[33] = ftlb[2] ^ r[32];
                r[(ins >> 21) & 0x1F] = ram->ReadMemory8(r[33]);
                break;

            case 0x24:
                // lbs
                r[32] = r[(ins >> 16) & 0x1F] + ((ins << 16) >> 16);
                if (0 != ((ftlbcheck[3] ^ r[32]) >> 13))
                {
                    r[33] = this->DTLBLookup(r[32], false);
                    if (r[33] == -1)
                    {
                        break;
                    }
                    ftlbcheck[3] = r[32];
                    ftlb[3] = ((r[33] ^ r[32]) >> 13) << 13;
                }
                r[33] = ftlb[3] ^ r[32];
                r[(ins >> 21) & 0x1F] = ((ram->ReadMemory8(r[33])) << 24) >> 24;
                break;

            case 0x25:
                // lhz
                r[32] = r[(ins >> 16) & 0x1F] + ((ins << 16) >> 16);
                r[33] = this->DTLBLookup(r[32], false);
                if (r[33] == -1)
                {
                    break;
                }
                r[(ins >> 21) & 0x1F] = (ushort)ram->ReadMemory16(r[33]);
                break;

            case 0x26:
                // lhs
                r[32] = r[(ins >> 16) & 0x1F] + ((ins << 16) >> 16);
                r[33] = this->DTLBLookup(r[32], false);
                if (r[33] == -1)
                {
                    break;
                }
                r[(ins >> 21) & 0x1F] = (ram->ReadMemory16(r[33]) << 16) >> 16;
                break;

            case 0x27:
                // addi signed
                imm = (ins << 16) >> 16;
                rA = r[(ins >> 16) & 0x1F];
                rindex = (ins >> 21) & 0x1F;
                r[rindex] = rA + imm;
                //this->SR_CY = r[rindex] < rA;
                //this->SR_OV = (((rA ^ imm ^ -1) & (rA ^ r[rindex])) & 0x80000000)?true:false;
                //TODO overflow and carry
                // maybe wrong
                break;

            case 0x29:
                // andi
                r[(ins >> 21) & 0x1F] = r[(ins >> 16) & 0x1F] & (ins & 0xFFFF);
                break;


            case 0x2A:
                // ori
                r[(ins >> 21) & 0x1F] = r[(ins >> 16) & 0x1F] | (ins & 0xFFFF);
                break;

            case 0x2B:
                // xori
                rA = r[(ins >> 16) & 0x1F];
                r[(ins >> 21) & 0x1F] = rA ^ ((ins << 16) >> 16);
                break;

            case 0x2D:
                // mfspr
                r[(ins >> 21) & 0x1F] = this->GetSPR((r[(ins >> 16) & 0x1F] | (ins & 0xFFFF)));
                break;

            case 0x2E:
                switch ((ins >> 6) & 0x3)
                {
                    case 0:
                        // slli
                        r[(ins >> 21) & 0x1F] = r[(ins >> 16) & 0x1F] << (ins & 0x1F);
                        break;
                    case 1:
                        // rori
                        r[(ins >> 21) & 0x1F] = (int)((uint)r[(ins >> 16) & 0x1F] >> (ins & 0x1F));
                        break;
                    case 2:
                        // srai
                        r[(ins >> 21) & 0x1F] = r[(ins >> 16) & 0x1F] >> (ins & 0x1F);
                        break;
                    default:
                        LOG("Error: opcode 2E not implemented");
                        abort();
                        break;
                }
                break;

            case 0x2F:
                // sf...i
                imm = (ins << 16) >> 16;
                switch ((ins >> 21) & 0x1F)
                {
                    case 0x0:
                        // sfnei
                        this->SR_F = (r[(ins >> 16) & 0x1F] == imm) ? true : false;
                        break;
                    case 0x1:
                        // sfnei
                        this->SR_F = (r[(ins >> 16) & 0x1F] != imm) ? true : false;
                        break;
                    case 0x2:
                        // sfgtui
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] > (uint)imm;
                        break;
                    case 0x3:
                        // sfgeui
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] >= (uint)imm;
                        break;
                    case 0x4:
                        // sfltui
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] < (uint)imm ? true : false;
                        break;
                    case 0x5:
                        // sfleui
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] <= (uint)imm ? true : false;
                        break;
                    case 0xa:
                        // sfgtsi
                        this->SR_F = (r[(ins >> 16) & 0x1F] > imm) ? true : false;
                        break;
                    case 0xb:
                        // sfgesi
                        this->SR_F = (r[(ins >> 16) & 0x1F] >= imm) ? true : false;
                        break;
                    case 0xc:
                        // sfltsi
                        this->SR_F = (r[(ins >> 16) & 0x1F] < imm) ? true : false;
                        break;
                    case 0xd:
                        // sflesi
                        this->SR_F = (r[(ins >> 16) & 0x1F] <= imm) ? true : false;
                        break;
                    default:
                        LOG("Error: sf...i not supported yet");
                        abort();
                        break;
                }
                break;

            case 0x30:
                // mtspr
                imm = (ins & 0x7FF) | ((ins >> 10) & 0xF800);
                this->SetSPR((r[(ins >> 16) & 0x1F] | imm), r[(ins >> 11) & 0x1F]);
                break;

            case 0x32:
                // floating point
                rA = (ins >> 16) & 0x1F;
                rB = (ins >> 11) & 0x1F;
                rD = (ins >> 21) & 0x1F;
                switch (ins & 0xFF)
                {
                    case 0x0:
                        // lf.add.s
                        f[rD] = f[rA] + f[rB];
                        break;
                    case 0x1:
                        // lf.sub.s
                        f[rD] = f[rA] - f[rB];
                        break;
                    case 0x2:
                        // lf.mul.s
                        f[rD] = f[rA] * f[rB];
                        break;
                    case 0x3:
                        // lf.div.s
                        f[rD] = f[rA] / f[rB];
                        break;
                    case 0x4:
                        // lf.itof.s
                        f[rD] = r[rA];
                        break;
                    case 0x5:
                        // lf.ftoi.s
                        r[rD] = (int)f[rA];
                        break;
                    case 0x7:
                        // lf.madd.s
                        f[rD] += f[rA] * f[rB];
                        break;
                    case 0x8:
                        // lf.sfeq.s
                        this->SR_F = (f[rA] == f[rB]) ? true : false;
                        break;
                    case 0x9:
                        // lf.sfne.s
                        this->SR_F = (f[rA] != f[rB]) ? true : false;
                        break;
                    case 0xa:
                        // lf.sfgt.s
                        this->SR_F = (f[rA] > f[rB]) ? true : false;
                        break;
                    case 0xb:
                        // lf.sfge.s
                        this->SR_F = (f[rA] >= f[rB]) ? true : false;
                        break;
                    case 0xc:
                        // lf.sflt.s
                        this->SR_F = (f[rA] < f[rB]) ? true : false;
                        break;
                    case 0xd:
                        // lf.sfle.s
                        this->SR_F = (f[rA] <= f[rB]) ? true : false;
                        break;
                    default:
                        LOG("Error: lf. " << (ins & 0xFF) << " not supported yet");
                        abort();
                        break;
                }
                break;

            case 0x35:
                // sw
                imm = ((((ins >> 10) & 0xF800) | (ins & 0x7FF)) << 16) >> 16;
                r[32] = r[(ins >> 16) & 0x1F] + imm;
                if (0 != (r[32] & 0x3))
                {
                    LOG("Error in sw: no aligned memory access");
                    abort();
                }
                if (0 != ((ftlbcheck[4] ^ r[32]) >> 13))
                {
                    r[33] = this->DTLBLookup(r[32], true);
                    if (r[33] == -1)
                    {
                        break;
                    }
                    ftlbcheck[4] = r[32];
                    ftlb[4] = ((r[33] ^ r[32]) >> 13) << 13;
                }
                r[33] = ftlb[4] ^ r[32];
                if (r[33] > 0)
                {
                    int32mem[r[33] >> 2] = r[(ins >> 11) & 0x1F];
                }
                else
                {
                    ram->WriteMemory32(r[33], r[(ins >> 11) & 0x1F]);
                }
                break;


            case 0x36:
                // sb
                imm = ((((ins >> 10) & 0xF800) | (ins & 0x7FF)) << 16) >> 16;
                r[32] = r[(ins >> 16) & 0x1F] + imm;
                if (0 != ((ftlbcheck[5] ^ r[32]) >> 13))
                {
                    r[33] = this->DTLBLookup(r[32], true);
                    if (r[33] == -1)
                    {
                        break;
                    }
                    ftlbcheck[5] = r[32];
                    ftlb[5] = ((r[33] ^ r[32]) >> 13) << 13;
                }
                r[33] = ftlb[5] ^ r[32];
                ram->WriteMemory8(r[33], (byte)r[(ins >> 11) & 0x1F]);
                break;

            case 0x37:
                // sh
                imm = ((((ins >> 10) & 0xF800) | (ins & 0x7FF)) << 16) >> 16;
                r[32] = r[(ins >> 16) & 0x1F] + imm;
                r[33] = this->DTLBLookup(r[32], true);
                if (r[33] == -1)
                {
                    break;
                }
                ram->WriteMemory16(r[33], (short)r[(ins >> 11) & 0x1F]);
                break;

            case 0x38:
                // three operands commands
                rA = r[(ins >> 16) & 0x1F];
                rB = r[(ins >> 11) & 0x1F];
                rindex = (ins >> 21) & 0x1F;
                switch (ins & 0x3CF)
                {
                    case 0x0:
                        // add signed
                        r[rindex] = rA + rB;
                        //this->SR_CY = r[rindex] < rA;
                        //this->SR_OV = (((rA ^ rB ^ -1) & (rA ^ r[rindex])) & 0x80000000)?true:false;
                        //TODO overflow and carry
                        break;
                    case 0x2:
                        // sub signed
                        r[rindex] = rA - rB;
                        //TODO overflow and carry
                        //this->SR_CY = (rB > rA);
                        //this->SR_OV = (((rA ^ rB) & (rA ^ r[rindex])) & 0x80000000)?true:false;
                        break;
                    case 0x3:
                        // and
                        r[rindex] = rA & rB;
                        break;
                    case 0x4:
                        // or
                        r[rindex] = rA | rB;
                        break;
                    case 0x5:
                        // or
                        r[rindex] = rA ^ rB;
                        break;
                    case 0x8:
                        // sll
                        r[rindex] = rA << (rB & 0x1F);
                        break;
                    case 0x48:
                        // srl not signed
                        r[rindex] =(int) (((uint)rA) >> (rB & 0x1F));
                        break;
                    case 0xf:
                        // ff1
                        r[rindex] = 0;
                        for (i = 0; i < 32; i++)
                        {
                            if (0 != (rA & (1 << i)))
                            {
                                r[rindex] = i + 1;
                                break;
                            }
                        }
                        break;
                    case 0x88:
                        // sra signed
                        r[rindex] = rA >> (rB & 0x1F);
                        // be carefull here and check
                        break;
                    case 0x10f:
                        // fl1
                        r[rindex] = 0;
                        for (i = 31; i >= 0; i--)
                        {
                            if (0 != (rA & (1 << i)))
                            {
                                r[rindex] = i + 1;
                                break;
                            }
                        }
                        break;
                    case 0x306:
                        // mul signed (specification seems to be wrong)
                        {
                            //TODO: check this lol
                            // this is a hack to do 32 bit signed multiply. Seems to work but needs to be tested.
                            r[rindex] = rA * rB;
                            auto rAl = rA & 0xFFFF;
                            auto rBl = rB & 0xFFFF;
                            r[rindex] = (int)((r[rindex] & 0xFFFF0000) | (uint)((rAl * rBl) & 0xFFFF));
                            auto result = rA * rB;
                            this->SR_OV = (result < (-2147483647 - 1)) || (result > (2147483647));
                            auto uresult = (uint)(rA) * (uint)(rB);
                            this->SR_CY = (uresult > (4294967295));
                        }
                        break;
                    case 0x30a:
                        // divu (specification seems to be wrong)
                        this->SR_CY = rB == 0;
                        this->SR_OV = false;
                        if (!this->SR_CY)
                        {
                            r[rindex] = /*Math.floor*/ (int) ((uint)rA / (uint)rB);
                        }
                        break;
                    case 0x309:
                        // div (specification seems to be wrong)
                        this->SR_CY = rB == 0;
                        this->SR_OV = false;
                        if (!this->SR_CY)
                        {
                            r[rindex] = rA / rB;
                        }

                        break;
                    default:
                        LOG("Error: op38 opcode not supported yet");
                        abort();
                        break;
                }
                break;

            case 0x39:
                // sf....
                switch ((ins >> 21) & 0x1F)
                {
                    case 0x0:
                        // sfeq
                        this->SR_F = (r[(ins >> 16) & 0x1F] == r[(ins >> 11) & 0x1F]) ? true : false;
                        break;
                    case 0x1:
                        // sfne
                        this->SR_F = (r[(ins >> 16) & 0x1F] != r[(ins >> 11) & 0x1F]) ? true : false;
                        break;
                    case 0x2:
                        // sfgtu
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] > (uint)r[(ins >> 11) & 0x1F];
                        break;
                    case 0x3:
                        // sfgeu
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] >= (uint)r[(ins >> 11) & 0x1F];
                        break;
                    case 0x4:
                        // sfltu
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] < (uint)r[(ins >> 11) & 0x1F];
                        break;
                    case 0x5:
                        // sfleu
                        this->SR_F = (uint)r[(ins >> 16) & 0x1F] <= (uint)r[(ins >> 11) & 0x1F];
                        break;
                    case 0xa:
                        // sfgts
                        this->SR_F = (r[(ins >> 16) & 0x1F] > r[(ins >> 11) & 0x1F]) ? true : false;
                        break;
                    case 0xb:
                        // sfges
                        this->SR_F = (r[(ins >> 16) & 0x1F] >= r[(ins >> 11) & 0x1F]) ? true : false;
                        break;
                    case 0xc:
                        // sflts
                        this->SR_F = (r[(ins >> 16) & 0x1F] < r[(ins >> 11) & 0x1F]) ? true : false;
                        break;
                    case 0xd:
                        // sfles
                        this->SR_F = (r[(ins >> 16) & 0x1F] <= r[(ins >> 11) & 0x1F]) ? true : false;
                        break;
                    default:
                        LOG("Error: sf.... supported yet");
                        abort();
                        break;
                }
                break;

            default:
                LOG("Error: Instruction with opcode " << (ins >> 26) << " not supported");
                abort();
                break;
        }

        this->pc = this->nextpc++;
        this->delayedins = false;

    } while (--steps > 0); // main loop
}

void CPU::Step()
{
    Step(1, 100);
}
